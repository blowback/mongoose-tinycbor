/*******************************************************************************

  (C) UTTO Technologies Ltd 2018, ...

FILE
    <<TODO>>

DESCRIPTION
    <<TODO>>

REFERENCES
    <<TODO>>
 *
 ******************************************************************************/

/*============================================================================*
 ANSI C & System-wide Header Files
 *============================================================================*/


/*============================================================================*
 Interface Header Files
 *============================================================================*/
/* None */

/*============================================================================*
 Local Header File
 *============================================================================*/
#include "mgos_tinycbor.h"

/*============================================================================*
 Public Data
 *============================================================================*/
/* None */

/*============================================================================*
 Private Defines
 *============================================================================*/
/* None */

/*============================================================================*
 Private Data Types
 *============================================================================*/
/* None */

/*============================================================================*
 Private Function Prototypes
 *============================================================================*/
/* None */

/*============================================================================*
 Private Data
 *============================================================================*/
/* None */

/*============================================================================*
 Public Function Implementations
 *============================================================================*/

/*----------------------------------------------------------------------------*
 * NAME
    <<TODO>>
 *
 * DESCRIPTION
    <<TODO>>
 *
 * RETURNS
    <<TODO>>
 *----------------------------------------------------------------------------*/
bool mgos_tinycbor_init(void) {
    return true;
}

/*============================================================================*
 Private Functions
 *============================================================================*/
/* None */

/*============================================================================*
 End Of File
 *============================================================================*/

